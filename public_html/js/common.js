$(document).ready(function() {
    var upload_path = $('.upload-path');
    $('.upload').on('change', function() {
        if(this.files != undefined){
            upload_path.html(this.files[0].name);
        }
        else{
            upload_path.html('error');
        }
    });
    $('.toggle-menu').on('click', function(e){
        e.preventDefault();
        var menu = $('.auth-menu');
        if(menu.hasClass('show')){
            menu.removeClass('show');
        }
        else{
            menu.addClass('show');
        }
    });
    
    $('.phone-mask').mask('+7(000)000-00-00');
    
    $('.checkbox-block').on('click', function(){
        $(this).find('input[type="checkbox"]').click();
        if($(this).find('input[type="checkbox"]').is(':checked')){
            $(this).addClass('checkbox-on');
        }
        else{
            $(this).removeClass('checkbox-on');
        }
    });
    
    $('.input-radio-block').on('click', function(){
        var name = $(this).find('input[type="radio"]').attr('name');
        $('input[name="'+name+'"]').closest('.input-radio-block').removeClass('radio-on');
        $(this).find('input[type="radio"]').prop("checked", true);
        if($(this).find('input[type="radio"]').is(':checked')){
            $(this).addClass('radio-on');
        }
        else{
            $(this).removeClass('radio-on');
        }
    });
    
    if (typeof $.fn.datepicker !== 'undefined') {
        var datePicker = $( ".datepicker" );

        if(datePicker.length){
            datePicker.datepicker({
                closeText: "Закрыть",
                prevText: "&#x3C;Пред",
                nextText: "След&#x3E;",
                currentText: "Сегодня",
                monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь", "Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
                monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн", "Июл","Авг","Сен","Окт","Ноя","Дек" ],
                dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота" ],
                dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт" ],
                dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб" ],
                weekHeader: "Нед",
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                            changeMonth: true,
                            changeYear: true,
                //isRTL: false,
                //showMonthAfterYear: false,
                //yearSuffix: "",
                //yearRange: '1930:1998'
            });
        }
    }
    
    if (typeof $.fn.select2 !== 'undefined') {
        $('select').select2({minimumResultsForSearch: -1});
    }
    
    $('.fancybox').fancybox({minWidth: 550});
    $('.j7-fancybox-close').on('click', function(){
        $.fancybox.close();
    });
    
})

function j7_modal(id){
    $.fancybox.open({href: '#' + id, wrapCSS: 'modal-'+id});
}